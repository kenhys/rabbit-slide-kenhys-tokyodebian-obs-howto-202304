# Open Build Serviceを使ってみた話

subtitle
:  各種ディストリビューション向け\\nパッケージングへの活用

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2023年4月 東京エリア・関西合同Debian勉強会

allotted-time
:  25m

theme
:  .

# スライドはRabbit Slide Showにて公開済みです

* Debianパッケージング Open Build Service編
  * <https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-obs-howto-202304/>

# 本日の内容

* Open Build Serviceを試す機会があったのでその紹介
  * どんなことができるのか？
  * どう使ったらいいのか？
  * どういう用途におすすめなのか？(**個人の感想です**)

# Open Build Serviceとは

![](images/openbuildservice.png){:relative-height="80"}

* <https://openbuildservice.org/>

# Open Build Serviceとは

* 公式サイトは <https://openbuildservice.org/>
  * openSUSEの開発に利用されているソフトウェア
    * 幅広い環境向けにパッケージのビルドサービスを提供できるのが特徴
  * OBSのインスタンスが <https://build.opensuse.org/>

# build.opensuse.org

![](images/buildopensuse.png){:relative-height="80"}

* <https://build.opensuse.org/>

# Open Build Serviceを知るには

* 第1回 Open Build Service道場
  * <https://www.slideshare.net/ftake/1-open-build-service>
  * 2012年の資料ではあるものの参考になるはず

# Open Build Serviceを知るには

* Open Build Service道場
  * <https://www.slideshare.net/ftake/obs-dojoadv>
  * パッケージの新規作成編 **作りかけバージョン**
  * 2014年の資料

# どんなことができるのか？

* ✅debやrpmのビルドができる
  * Debian, Ubuntu, Fedora, openSUSE
* ✅ビルドしたパッケージのリポジトリを公開できる

# 類似サービスとの違い

* packagecloud.io
* launchpad.net

# 類似サービスとの違い(1)

* <https://packagecloud.io>
  * ✅リポジトリのホスティングを提供
  * ❌パッケージのビルドは自前で行う必要がある

# 類似サービスとの違い(2)

* <https://launchpad.net>
  * ✅リポジトリのホスティングを提供
  * ✅パッケージのビルドもおまかせ
  * ❌サポートされているUbuntuのリリースのみ

# Open Build Service

* 便利なところは？
* 気になるところは？

# 便利なところ(1)

* ✅RPM系だけでなく、Debもサポートしている
* ✅ソースパッケージをアップロードすると、あとはおまかせにできる
  * ✅パッケージの署名用の鍵のメンテナンス不要

# 便利なところ(2)

* ✅ミラーを利用できる
  * <https://en.opensuse.org/MirrorCache>
  * ✅<https://mirrorcache.opensuse.org> を指定してあれば、適宜適切なミラーが自動的に選択される
    * 日本の場合は <https://mirrorcache-jp.opensuse.org>
* ✅プロジェクトのroleを設定することで複数人でメンテナンスできる

# 便利なところ(3)

* ✅ソースパッケージまで準備しておけばあとはoscコマンドを使って自動化できる
* ✅バージョン名を付与したパッケージを作ることで複数バージョンを公開できる
* ✅いわゆる独立したプロジェクトとしてメンテナンスすることもできる(はず) **要確認**
  * 個人のリポジトリは home:(ユーザー名)となる。公式扱いだと science:(xxx) みたいにできる

# 気になるところ(1)

* ❌<https://mirrorcache-jp.opensuse.org> へのミラーはそれほど早くない
  * 1日くらいみておくとよい
  * RPM系だと既定では <https://download.opensuse.org> への.repoがダウンロードできるようになっているので、ミラーを明示的に指定した場合にその影響を受ける

# 気になるところ(2)

* ❌独自プロジェクトにするには手間と時間がかかる
* ❌未対応のディストリビューションもある
  * 例: AmazonLinux

# OBSを利用するための準備

* 最初にすること
* home:プロジェクトの準備
* プロジェクトの設定
* プロジェクトのフラグ設定
* パッケージの設定
* 参考: <https://en.opensuse.org/openSUSE:Build_Service_Tutorial>

# 最初にすること

* <https://build.opensuse.org/> にアカウントを作成する
  * 既定では `home:(ユーザー名)` プロジェクトが利用できる
* osc コマンドを利用できるようにする
  * sudo apt install -y osc

# home:(ユーザー名)プロジェクトの準備

* osc checkout home:(ユーザー名)でプロジェクトをチェックアウトする(SVNっぽい感じ)
  * 初回のチェックアウトでアカウント情報を入力することで~/.config/osc/oscrcが作られる
  * `home:(ユーザー名)`というディレクトリがそのまま作成される

# プロジェクトの設定

* プロジェクトのメタ情報
  * 対象ディストリビューションとひもづける

  ```
  osc meta prj home:(ユーザー名) --file=prj-meta.xml
  ```

# プロジェクトのメタ情報

  * リポジトリ名と依存ディストリビューションを指定

  ```
  <project name="home:(ユーザー名)">
  <title/>
  <description/>
  <person userid="kenhys" role="maintainer"/>
  <repository name="bullseye">
    <path project="Debian:11" repository="update"/>
    <path project="Debian:11" repository="standard"/>
    <arch>x86_64</arch>
    <arch>i586</arch>
    <arch>aarch64</arch>
  </repository>
  </project>
  ```

# 複数人でメンテナンスする場合

* roleをプロジェクトのメタ情報に含める

  ```
  <person userid="kenhys" role="maintainer"/>
  <person userid="kou" role="maintainer"/>
  ```

# プロジェクトのメタ情報

![](images/obs-meta.png){:relative-height="80"}

* <https://build.opensuse.org/projects/home:(ユーザー名)/meta>

# リポジトリ一覧

![](images/obs-repositories.png){:relative-height="80"}

* <https://build.opensuse.org/repositories/home:(ユーザー名)>


# ディストリビューションの対応

* どれくらいのディストリビューションに対応しているのか？

  ```
  $ osc dists
  ```

# プロジェクトのフラグ

* 特定のディストリビューションで必要
  * AlmaLinux:8などのようにAppStreamでモジュールとして各種パッケージが提供されている場合(dnf module listで対象を確認できる)、OBSでは既定でモジュールは有効になっていない
  * AppStreamかつモジュールのパッケージをspecの`BuildRequires:`に指定するとunresolvableとなってビルドすらされない。
    * nothing provides: xxxxとそっけないメッセージがでるだけ

# 特定のモジュールを有効にするには

* プロジェクトのフラグを設定する
  * 例: osc meta prjconf home:kenhys --file prjconf.txt

  ```
  ExpandFlags: module:ruby-3.1
  ```

  * 参考: <https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.prjconfig.html>

# プロジェクトのフラグ

![](images/obs-prjconf.png){:relative-height="80"}

* <https://build.opensuse.org/projects/home:(ユーザー名)/prjconf>

# パッケージごとにすること

* パッケージのメタ情報の設定
  * 特定のディストリビューションに対応していない場合、パッケージごと無効にできる
    * プロジェクトのメタデータで設定したリポジトリ名を指定する
    * `<build>`のターゲットとして`<disable>`を指定する

  ```
  $ osc meta pkg hello-2.11 --file hello2.11-meta.xml
  ```

# パッケージのメタ情報の例

  ```
  <package name="hello-2.11" project="home:kenhys">
    <title>Hello 2.11</title>
    <description/>
    <disable repository="bullseye"/>
    <disable repository="focal"/>
    <disable repository="jammy"/>
  </package>
  ```
  
# パッケージのメタ情報の例

![](images/obs-package-meta.png){:relative-height="80"}

* <https://build.opensuse.org/package/meta/home:(ユーザー名)/hello-2.11>

# OBSでパッケージをビルドする方法

* rpm編
* deb編

# .rpmのビルド方法

* SRPMから作成する方法
  * PackageCloudから移行するなら、SRPMをインポートするのがお手軽
* .specと関連するソースアーカイブから作成

# SRPMを使う場合

  * `home:(ユーザー名)` で次のコマンドを実行する

  ```
  $ osc importsrcpkg -n hello-2.10 hello-2.10.src.rpm
  $ cd hello-2.10
  $ osc add hello.spec
  $ osc add hello-2.10.tar.gz
  $ osc commit -m "Add 2.10"
  ```

# .specや各種ソースアーカイブを使う場合
 
  * osc metaでパッケージに関するメタ情報を追加し、アーカイブを追加する

  ```
  $ osc meta pkg home:(ユーザー名) hello-2.10 --file=hello2.10-meta.xml
  $ osc update
  $ cd hello-2.10
  $ osc add (.specとか.tar.gzとか)
  $ osc commit -m "Add 2.10"
  ```

# パッケージのメタ情報設定

  ```
  <package name="hello-2.10" project="home:kenhys">
    <title>Hello 2.10</title>
    <description>GNU Hello</description>
  </package>
  ```

# .debのビルド方法

* .dsc, orig.tar.gz, debian.tar.xzを追加する

  ```
  $ cd hello-2.10
  $ osc add hello_2.10-3.dsc
  $ osc add hello_2.10-3.debian.tar.xz
  $ osc add hello_2.10.orig.tar.gz
  $ osc commit -m "Add 2.10"
  ```

# ビルド状態の確認

![](images/obs-build-results.png){:relative-height="80"}

* osc resultsでも確認できる

# リポジトリの利用方法

* .rpm
* .deb

# 例: AlmaLinux:8

![](images/obs-almalinux8.png){:relative-height="80"}

* .repoが公開されているのでそれを利用

# rpmの場合

* /etc/yum.repo.dに配置する

  ```
  $ cd /etc/yum.repo.d
  $ sudo curl -L -O https://mirrorcache-jp.opensuse.org/ \
    repositories/home:/(ユーザー名)/(ディストリビューション)/home:(ユーザー名).repo
  ```

# 例: Bullseye

![](images/obs-bullseye.png){:relative-height="80"}

* Release.keyが公開されているのでそれを利用


# debの場合

  * Release.keyが公開されているので、keyringに変換する

  ```
  $ curl -L -O \
    https://mirrorcache-jp.opensuse.org/repositories/home:/(ユーザー名)/bullseye/Release.key
  $ gpg --no-default-keyring --keyring ./archive-hello-keyring.gpg --import Release.key
  $ sudo mv archive-hello-keyring.gpg /usr/share/keyrings/
  ```

# debの場合(2)

* .listを追加してリポジトリへの参照を追加する

  ```
  # cat /etc/apt/sources.list.d/hello.list
  deb [signed-by=/usr/share/keyrings/archive-hello-keyring.gpg] \
    http://mirrorcache-jp.opensuse.org/repositories/home:(ユーザー名)/(ディストリビューション) /
  ```

# ハマりポイント(1)

* OBSでパッケージのビルドがはじまらない
  * ❌ メタデータに対象リポジトリが不足していると失敗します
  * ✅ 不足しているメタデータを追加する
  * ✅ 不足しているプロジェクトのフラグを追加する

# ハマりポイント(2)

* DockerではビルドできるのにOBSではビルドできない
  * ❌ rootユーザーでのビルドを前提していると失敗します
  * ✅ 一般ユーザーでビルドできるように修正する

# ハマりポイント(3)

* 複数バージョンを公開したいがビルドされなくなった
  * ❌ バージョンつけずに複数ソースをコミットした
  * ✅ バージョンつけてパッケージを管理する

* 例: helloディレクトリ配下に複数バージョンのソースをコミットすると正常にパッケージがビルドされなくなる

# ハマりポイント(4)

* homeじゃないプロジェクトの作成難易度が高い
  * ✅<https://build.opensuse.org/project/new> で新規プロジェクトを作成できる
  * ❌home:配下しか作成できない

    * `science:`とか`server:mail`などのカテゴリがあるが、あくまでそちらはopenSUSEの開発で使われるプロジェクト向け
    *  他の組織がビルドサービスを利用する場合には`isv:`配下にプロジェクトを作るのがおすすめ

# homeじゃないプロジェクトの作成難易度が高い問題

  * ❌<https://build.opensuse.org/project/new> では上記に該当しないプロジェクトを作成する場合にはメールで連絡をとるように案内されているが、その連絡先は <noreply@opensuse.org> となっており、メールを受け付けるつもりがない

  * ❌既知の問題 <https://github.com/openSUSE/open-build-service/issues/12356> だが修正されない

# homeじゃないプロジェクトの作成難易度が高い問題

  * <admin@opensuse.org> に連絡すると <https://progress.opensuse.org/> にissueを作ってもらえたりするが opensuse-admin-obs にアサインしてもらえないと気づかれない
  * OBSとはチームが別なので気づいてもらえるの待ち

# どんな用途に向くのか？

* 自分が書いたソフトウェアを様々なディストリビューション向けに一括で提供したい
* バージョンアップの頻度が高く各ディストリビューションのお作法にならってパッケージングがしんどい
* パッケージリポジトリの維持管理に手間をかけられない
  * 自分がupstream authorなソフトウェアならisv:配下にプロジェクトを作成するのがおすすめ
    * プロジェクト作成までは時間がかかるのは覚悟すべし
